import 'primeicons/primeicons.css';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import 'primereact/resources/primereact.min.css';
import 'primereact/resources/themes/omega/theme.css';
import React, { Component } from 'react';
import './App.css';

class App extends Component {

  state  = {
    equipamentos: []
  }

  componentWillMount() {
    this.buscar();
    setInterval(this.buscar, 30000);
  }

  buscar = () => {
    fetch("http://localhost:8080/equipamentos", {method: "GET"}).then(resposta => resposta.json().then(equipamentos => this.setState({equipamentos})));
  }

  render() {
    return (
      <div>
          <DataTable value={this.state.equipamentos}>
            <Column header="Descrição" field="descricao" />
            <Column header="IP" field="ip" />
            <Column header="Falhas" body={(equipamento) => Number(equipamento.falha * 100).toFixed(2)} />
          </DataTable>
        </div>
    );
  }

}

export default App;
